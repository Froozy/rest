package rest.dto;


import org.json.simple.JSONObject;

class JsonEncodeDemo {

   public static void main(String[] args){
      JSONObject obj = new JSONObject();
      obj.put("Id", 1);
      obj.put("Imie", "Johnny");
      obj.put("Nazwisko","Bananowy");
      obj.put("Wiek",36);

      System.out.print(obj);
   }
}