package rest.dto;

public class PersonDto {

	private Long Id;
	private String Imie;
	private String Nazwisko;
	private int Wiek;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getImie() {
		return Imie;
	}
	public void setImie(String imie) {
		Imie = imie;
	}
	public String getNazwisko() {
		return Nazwisko;
	}
	public void setNazwisko(String nazwisko) {
		Nazwisko = nazwisko;
	}
	public int getWiek() {
		return Wiek;
	}

	public void setAge(int wiek) {
		Wiek = wiek;
	}
	
	
	
}
