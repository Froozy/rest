package rest.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import domain.Person;
import domain.PersonDB;
import rest.dto.PersonDto;
import rest.dto.PersonDto;

@Path("profile")
public class PersonService {

	private Map<Long,Person> profiles = PersonDB.getProfiles();
	
	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response add(PersonDto dto){
		
		Person p = new Person();
		PersonDB.profiles.put((long)(PersonDB.profiles.size()+1),p);
		
		return Response.status(201).build();
	}
	/*
	@POST
	@Path("/Add{/PROFILE}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public profile addProfile(@PathParam("PROFILE")profile newprofile){
		//profile newprofile = new profile(3L,"Bogdan","Robolski");
		return profileservice.addprofile(newprofile);
		//newprofile;
	}
	*/
	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<PersonDto> getAllofThem(){
		List<PersonDto> result = new ArrayList<PersonDto>();
		for(int x = 1;x<=profiles.size();x++){
			Person p = profiles.get(x);
			PersonDto dto = new PersonDto();
			dto.setImie(p.getImie());
			dto.setId(p.getId());
			dto.setNazwisko(p.getNazwisko());
			dto.setAge(p.getWiek());
			result.add(dto);
		}
		return result;
		
	}
}
