package domain;

public class Person{
		
		private long Id;
		private String Imie;
		private String Nazwisko;
		private int Wiek;
		
		public Person()
		{
			
		}
		public Person(long id,String imie,String nazwisko){
			Id = id;
			Imie = imie;
			Nazwisko = nazwisko;
		}

		public long getId() {
			return Id;
		}
		public void setId(long id) {
			Id = id;
		}
		public String getImie() {
			return Imie;
		}
		public void setImie(String imie) {
			Imie = imie;
		}
		public String getNazwisko() {
			return Nazwisko;
		}
		public void setNazwisko(String nazwisko) {
			Nazwisko = nazwisko;
		}
		public int getWiek() {
			return Wiek;
		}
		public void setWiek(int wiek) {
			Wiek = wiek;
		}
		
	}
