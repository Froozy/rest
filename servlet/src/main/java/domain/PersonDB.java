package domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PersonDB {
	
	public static Map<Long,Person> profiles;
	
	public static Map<Long,Person> getProfiles(){
		return profiles;
	}
	public List<Person> getFromAge(int n1, int n2)
	{
		List<Person> list = new ArrayList<Person>();
		for(int x = 1;x<=profiles.size();x++){
		Person p = profiles.get(x);
			if(p.getWiek()>=n1 && p.getWiek()<=n2)
				{
					list.add(p);
				}
		}
		return list;
	}
	
	
}
